import axios from 'axios'

export default {
    namespaced:true,
    state:{
        allusers:[]
    },
    getters:{
        get_all_users(state){
            return state.allusers
        }
    },
    mutations:{
       SET_ALL_USERS(state,value){
            state.allusers = value
       }
    },
    actions:{
        async getUsersAll({dispatch}){
            console.log('solicitando todos los usuarios')
            await axios.get('/sanctum/csrf-cookie')
            const ct = await axios.get('/api/user')
            return dispatch('meUsers',ct.data)

        },

        meCategory({commit},data){
            console.log(data)
            commit('SET_ALL_USERS',data)
        }
    }
}

