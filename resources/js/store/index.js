import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'
import post from './post'
import category from './category'
import comment from './comment'
import user from './user'
Vue.use(Vuex)
export default new Vuex.Store({
    modules:{
        auth,
        post,
        category,
        comment,
        user
        //aqui los demas modulos
    }
})
